<p align="center">
    <img width="350" src=".\static\logo.png">
</p>

# DEMO PUSH - PYTHON 

## Name
    DEMO PUSH - PYTHON

## Description
    Esse projeto está sendo desenvolvido para descomplicar a integração e desenvolvimento com o protocolo PUSH.


## Visuals

    ...

## Installation

    ...

## Usage
    Python 3.10
    Flask 1.1.0

## Support
    Caso surja alguma dúvida referente a DEMO e ao protocolo PUSH envie um e-mail para:

    gustavo.henrique@zkteco.com
    bruno@zkteco.com
## Author
    Gustavo Henrique

## License
    Todos os direitos reservados para ZKTeco Brasil

## Project status
    O projeto foi iniciado em 03-10-2022 e está em andamento, aguarde para mais novidades!