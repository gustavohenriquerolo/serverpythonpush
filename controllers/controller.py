from models.server import Server
from models.server import app
from flask import request,send_file
from datetime import datetime
import os

# instancias
server = Server()


# Rotas ao qual os dispositivos PUSH ZKTeco vão enviar requisições
@app.route('/iclock/<param>', methods=['GET', 'POST'])
def device(param):
    # todas as mensagens enviadas pelo equipamento possuem SN
    serial_number = request.args.get('SN')

    # quando for POST, le os dados postados
    post_data = None
    if request.method == 'POST':
        post_data = request.data

    # inicio de comunicação
    match param:

        case 'cdata':  # ACC Push envia um pedido de início de comunicação
            if request.method == 'GET':
                return 'OK'
                # ouviu a requisição do mesmo
            else:
                print(request.data)
                return 'OK'

        case 'registry':
            # se o equipamento é conhecido, retorna o RegistryCode
            if serial_number in server.list_devices:
                return f'RegistryCode={server.list_devices[serial_number]}\n'

            # se é um equipamento ainda não conhecido,
            # insere na lista de equipamentos conhecidos, gera um RegistryCode e
            # retorna o registry code. O equipamento passa a ser "conhecido" ou "registrado"

            server.add_device(serial_number)
            number = server.list_devices[serial_number]
            return f'RegistryCode={number}'

        case 'getrequest':
            if serial_number not in server.list_devices:  # caso o equipamento não esteja registrado
                return "406"  # Vai ser retornado um erro 406
           
            # tem comando para ser executado?
            if len(server.list_commands) > 0:
                # retorna para o equipamento uma lista com todos os comandos
                for i in server.list_commands:
                    cmd = i
                    server.remove_command(cmd)
                    return cmd

            return 'Ok'

        case 'ping':
            """ 
                Durante o upload de dados massivos, o comando ping é usado para verificar se a conexão
                não foi perdida. Assim que é finalizado a troca de dados o comando getrequest retorna.
            """
            return 'OK'

        case 'push':
            """
            Depois que o código de registro for retornado após a solicitação de registro ser bem-sucedida na etapa anterior, o
            dispositivo precisa obter ativamente os parâmetros de configuração do servidor e, em seguida, toda a inicialização
             processo está terminado.
            """
            cmd = "ServerVersion=3.0.1\n"
            cmd += "ServerName=ADMS\n"
            cmd += "PushVersion=3.0.1\n"
            cmd += "ErrorDelay=5\n"
            cmd += "RequestDelay=1\n"
            cmd += "TransTimes=00:00;14:00\n"
            cmd += "TransInterval=1\n"
            cmd += "TransTables=User Transaction\n"
            cmd += "Realtime=1\n"
            cmd += "SessionID=30BFB04B2C8AECC72C01C03BFD549D15\n"
            cmd += "TimeoutSec=10\n"
            return cmd

        case 'devicecmd':
            """
            Informa o resultado retornado do comando enviando para o dispositivo.
            """
            print(request.data)
            return "OK"

        case 'rtdata':
            """
           O equipamento está solicitando a hora atual para o servidor
            """
            
            return DataTime() # Essa função retorna a data e hora em segundos seguindo o calculo que consta na documentação

        case 'querydata':
            """
            Quando ocorre um comando de query o dispositivo informa os dados
            """
            data = request.data
            print(data)
            return 'OK'


# Rotas utilizadas para adicionar comandos na fila utilizando o Postman
@app.route('/device/command', methods=['GET', 'POST', 'DELETE'])
def device_command():

    post_data = request.data
    server.add_command(post_data)
    return "OK"



# Há duas formas de gerar um template facial para um usuário.A primeira você faz o envio
# da foto em base64 como no exemplo no postman. E a segunda foram que é passado uma url
# onde a foto vai estár disponivel. Essa rota vais er utilizada caso o dev decida utilizar
# o envio da url
@app.route('/user/photo/<param>')
def getUserPhoto(param):

    # Obtém o diretório atual em que o script está sendo executado.
    current_directory  = os.path.dirname(os.path.abspath(__file__))
    # Navega para o diretório acima do diretório atual.
    parent_directory = os.path.abspath(os.path.join(current_directory , os.pardir))
    # O caminho para o arquivo na pasta acima do diretório atual.
    file_path = os.path.join(parent_directory, f'image\\{param}')

    return send_file(file_path, mimetype='image/jpeg')


# # Essa função retorna a data e hora em segundos seguindo o calculo que consta na documentação.
def DataTime():

    # Obter a data e hora atual
    data_hora_atual = datetime.now()
    # Obter o ano, mês e dia
    year = data_hora_atual.year
    mon = data_hora_atual.month
    day = data_hora_atual.day

    # Obter a hora, minuto e segundo
    hour = data_hora_atual.hour
    min = data_hora_atual.minute
    sec = data_hora_atual.second

    tt = ((year - 2000)* 12 * 31 + ((mon - 1) * 31) + day - 1) * (24 * 60 * 60) + ((hour + 3) * 60 + min) * 60 + sec
    return f'DateTime={tt},MachineTZ=-0300,ServerTZ=-0300'

