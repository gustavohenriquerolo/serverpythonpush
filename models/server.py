from flask import Flask
import random

app = Flask(__name__)


class Server:
    list_commands = []
    list_devices = {}

    def start_server(self):
        app.run(port=8077, host='0.0.0.0', debug=True)
        return 'OK'

    def add_command(self, comando):
        self.list_commands.append(comando)
        pass

    def remove_command(self, comando):
        self.list_commands.remove(comando)

    def add_device(self, sn):
        self.list_devices[sn] = self.random_cod()
        

    def random_cod(self):
        while True:
            number = random.randint(1000000000, 4294967295)
            if not number in self.list_devices.values():
                return number
